STM32_fiddling
==============

Place to put code while fiddling with STM32 microcontrollers.

Guides
------

https://fishpepper.de/2016/09/16/installing-using-st-link-v2-to-flash-stm32-on-linux/
https://github.com/pyocd/pyOCD

